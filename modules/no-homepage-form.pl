# Copyright (C) 2019  Aki Goto <tyatsumi@protonmail.ch>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.

use strict;
use v5.10;

AddModuleDescription('no-homepage-form.pl', 'No Homepage Form Extension');

our ($CommentsPattern, $q, $EditNote, @MyFormChanges);
our (@MyInitVariables, @MyFooters);

*OldGetCommentForm = \&GetCommentForm;
*GetCommentForm = \&NewNHFGetCommentForm;

sub NewNHFGetCommentForm {
  my ($id, $rev, $comment) = @_;
  if ($CommentsPattern ne '' and $id and $rev ne 'history' and $rev ne 'edit'
      and $id =~ /$CommentsPattern/ and UserCanEdit($id, 0, 1)) {
    my $html = $q->div({-class=>'comment'},
		       GetFormStart(undef, undef, 'comment'),
		       $q->p(GetHiddenValue('title', $id),
			     $q->label({-for=>'aftertext', -accesskey=>T('c')},
				       T('Add your comment here:')), $q->br(),
			     GetTextArea('aftertext', $comment, 10)),
		       $EditNote,
		       $q->p($q->span({-class=>'username'},
				      $q->label({-for=>'username'}, T('Username:')), ' ',
				      $q->textfield(-name=>'username', -id=>'username',
						    -default=>GetParam('username', ''),
						    -override=>1, -size=>20, -maxlength=>50))),
		       $q->p($q->submit(-name=>'Save', -accesskey=>T('s'), -value=>T('Save')), ' ',
			     $q->submit(-name=>'Preview', -accesskey=>T('p'), -value=>T('Preview'))),
		       $q->end_form());
    foreach my $sub (@MyFormChanges) {
      $html = $sub->($html, 'comment');
    }
    return $html;
  }
  return '';
}

push(@MyInitVariables, \&NHFInitVariables);

sub NHFInitVariables {
  @MyFooters = (\&GetCommentForm, \&WrapperEnd, \&DefaultFooter);
}
