# x-forwarded-for.pl - fix remote IP address for reverse proxy
#
# Copyright (C) 2014 Aki Goto <tyatsumi@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

AddModuleDescription('x-forwarded-for.pl', 'X-Forwarded-For Extension');

*OldXForwardedForGetRemoteAddress = *GetRemoteAddress;
*GetRemoteAddress = *NewXForwardedForGetRemoteAddress;

sub NewXForwardedForGetRemoteAddress {
  if ($ENV{HTTP_X_FORWARDED_FOR}) {
    my @list = split /,\s*/, $ENV{HTTP_X_FORWARDED_FOR};
    for my $element (@list) {
      if ($element) {
        return $element;
      }
    }
  }
  return OldXForwardedForGetRemoteAddress();
}
